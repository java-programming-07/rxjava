# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.4/maven-plugin/reference/html/#build-image)
* [Spring Reactive Web](https://docs.spring.io/spring-boot/docs/2.7.4/reference/htmlsingle/#web.reactive)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a Reactive RESTFul Web Service](https://spring.io/guides/gs/reactive-rest-service/)

### Checkstyle

```sh
$ mvn checkstyle:check
```
- [Checkstyle in Maven link](https://maven.apache.org/plugins/maven-checkstyle-plugin/usage.html)

### Jacoco

http://localhost:63342/rxjava/target/site/jacoco/index.html

![img.png](addons/img.png)

### End points

- Customer: Get a new customer.
```sh
curl --location --request POST 'http://localhost:8090/customer' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "John Smith",
  "phoneType": "mobile",
  "phoneNumber": "918147663",
  "birthday": "1989-04-04"
}'
```

### OpenApi3 generator

- [Swagger link](http://localhost:8090/swagger-ui/index.html)

- Importing our library.
```sh
<importMappings>
  <importMapping>
    SaveCustomerResponse=our.custom.library.SaveCustomerResponse
  </importMapping>
</importMappings>
```
