package com.jevillac07.rxjava.model.dto;

import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.rxjava.util.ReadFile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SaveCustomerRequestTest {
  private Validator validator;

  private SaveCustomerApiRequest request;

  @BeforeEach
  void init() {
    ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
    this.validator = vf.getValidator();
    this.request = ReadFile.readDataFromFile(
      "mock/save-customer-request.json", SaveCustomerApiRequest.class);
  }

  @Test
  @DisplayName("Validate name")
  void WrongName() {
    request.setName(null);

    Set<ConstraintViolation<SaveCustomerApiRequest>> violations = validator.validate(request);

    assertTrue(violations.size() > 0);
  }

  @Test
  @DisplayName("Validate phone type")
  void WrongPhoneType() {
    request.setPhoneType(null);

    Set<ConstraintViolation<SaveCustomerApiRequest>> violations = validator.validate(request);

    assertTrue(violations.size() > 0);
  }

  @Test
  @DisplayName("Validate phone number")
  void WrongPhoneNumber() {
    request.setPhoneNumber("91814766301");

    Set<ConstraintViolation<SaveCustomerApiRequest>> violations = validator.validate(request);

    assertTrue(violations.size() > 0);
  }

  @Test
  @DisplayName("Validate birthdays with a date after the current one")
  void WrongBirthday() {
    request.setBirthday("04/04/1989");

    Set<ConstraintViolation<SaveCustomerApiRequest>> violations = validator.validate(request);

    assertTrue(violations.size() > 0);
  }
}
