package com.jevillac07.rxjava.service;

import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.openapi.model.SaveCustomerApiResponse;
import com.jevillac07.rxjava.mapper.CustomerApiMapper;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyRequest;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyResponse;
import com.jevillac07.rxjava.repository.CustomerRepository;
import com.jevillac07.rxjava.service.imp.CustomerApiServiceImp;
import com.jevillac07.rxjava.util.ReadFile;
import io.reactivex.rxjava3.core.Single;
import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImpTest {
  @InjectMocks
  private CustomerApiServiceImp customerServiceImp;

  @Mock
  private CustomerApiMapper customerMapper;

  @Mock
  private CustomerRepository customerRepository;

  @Test
  @Order(1)
  @DisplayName("Consume service save customer")
  void saveCustomer() {
    SaveCustomerApiRequest request = new SaveCustomerApiRequest();
    SaveCustomerApiResponse response = ReadFile.readDataFromFile(
      "mock/save-customer-response.json", SaveCustomerApiResponse.class);
    SaveCustomerProxyRequest proxyRequest = new SaveCustomerProxyRequest();
    SaveCustomerProxyResponse proxyResponse = new SaveCustomerProxyResponse();

    when(customerMapper.saveCustomerApiToSaveCustomerProxy(any(SaveCustomerApiRequest.class)))
      .thenReturn(proxyRequest);

    when(customerRepository.saveCustomer(any(SaveCustomerProxyRequest.class)))
      .thenReturn(Single.just(proxyResponse));

    when(customerMapper.saveCustomerProxyToSaveCustomerApi(any(SaveCustomerProxyResponse.class)))
      .thenReturn(response);

    val testObserver = customerServiceImp.saveCustomer(
      request).test();

    testObserver.assertComplete();
    testObserver.assertValue(response);
  }

  @Test
  @Order(2)
  @DisplayName("Throwable error in the repository")
  void saveCustomerWithError() {
    SaveCustomerApiRequest request = new SaveCustomerApiRequest();
    SaveCustomerProxyRequest proxyRequest = new SaveCustomerProxyRequest();

    when(customerMapper.saveCustomerApiToSaveCustomerProxy(any(SaveCustomerApiRequest.class)))
      .thenReturn(proxyRequest);

    when(customerRepository.saveCustomer(any(SaveCustomerProxyRequest.class)))
      .thenReturn(Single.error(Throwable::new));

    val testObserver = customerServiceImp.saveCustomer(
      request).test();

    testObserver.assertNotComplete();
    testObserver.assertError(Throwable.class);
  }
}
