package com.jevillac07.rxjava.controller;

import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.openapi.model.SaveCustomerApiResponse;
import com.jevillac07.rxjava.service.CustomerApiService;
import com.jevillac07.rxjava.util.ReadFile;
import io.reactivex.rxjava3.core.Single;
import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {
  @InjectMocks
  private CustomerApiController customerController;

  @Mock
  private CustomerApiService customerService;

  @Test
  @Order(1)
  @DisplayName("Consume controller save customer")
  void saveCustomer() {
    SaveCustomerApiRequest request = new SaveCustomerApiRequest();
    SaveCustomerApiResponse response = ReadFile.readDataFromFile(
      "mock/save-customer-response.json", SaveCustomerApiResponse.class);

    when(customerService.saveCustomer(any(SaveCustomerApiRequest.class)))
      .thenReturn(Single.just(response));

    val testObserver = customerController.saveCustomer(
      request).test();

    testObserver.assertComplete();
    testObserver.assertValue(ResponseEntity.ok(response));
  }

  @Test
  @Order(2)
  @DisplayName("Save customer but there is an error in the service")
  void saveCustomerWithError() {
    SaveCustomerApiRequest request = new SaveCustomerApiRequest();

    when(customerService.saveCustomer(any(SaveCustomerApiRequest.class)))
      .thenReturn(Single.error(Throwable::new));

    val testObserver = customerController
      .saveCustomer(request).test();

    testObserver.assertNotComplete();
    testObserver.assertError(Throwable.class);
  }
}
