package com.jevillac07.rxjava.repository;

import com.jevillac07.rxjava.model.provider.SaveCustomerProxyRequest;
import com.jevillac07.rxjava.repository.imp.CustomerRepositoryImp;
import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CustomerRepositoryImpTest {
  @InjectMocks
  private CustomerRepositoryImp customerRepositoryImp;

  @Test
  @Order(1)
  @DisplayName("Consume a proxy save customer")
  void saveCustomer() {
    SaveCustomerProxyRequest request = new SaveCustomerProxyRequest();

    val testObserver = customerRepositoryImp.saveCustomer(
      request).test();

    testObserver.assertNotComplete();
  }
}
