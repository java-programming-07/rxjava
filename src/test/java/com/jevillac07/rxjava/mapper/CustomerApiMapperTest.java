package com.jevillac07.rxjava.mapper;

import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.openapi.model.SaveCustomerApiResponse;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyRequest;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyResponse;
import com.jevillac07.rxjava.util.ReadFile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerApiMapperTest {
  private final CustomerApiMapper customerMapper = Mappers.getMapper(CustomerApiMapper.class);

  @Test
  @Order(1)
  @DisplayName("Get SaveCustomerProxyRequest model")
  void saveCustomerToSaveCustomerProxy() {
    SaveCustomerApiRequest request = ReadFile.readDataFromFile(
      "mock/save-customer-request.json", SaveCustomerApiRequest.class);
    SaveCustomerProxyRequest responseMock = ReadFile.readDataFromFile(
      "mock/save-customer-proxy-request.json", SaveCustomerProxyRequest.class);

    SaveCustomerProxyRequest response = customerMapper.saveCustomerApiToSaveCustomerProxy(request);

    assertEquals(response.getPhone(), responseMock.getPhone());
  }

  @Test
  @Order(2)
  @DisplayName("Get SaveCustomerResponse model")
  void saveCustomerProxyToSaveCustomer() {
    SaveCustomerProxyResponse request = ReadFile.readDataFromFile(
      "mock/save-customer-proxy-response.json", SaveCustomerProxyResponse.class);
    SaveCustomerApiResponse responseMock = ReadFile.readDataFromFile(
      "mock/save-customer-response.json", SaveCustomerApiResponse.class);

    SaveCustomerApiResponse response = customerMapper.saveCustomerProxyToSaveCustomerApi(request);

    assertEquals(response, responseMock);
  }
}
