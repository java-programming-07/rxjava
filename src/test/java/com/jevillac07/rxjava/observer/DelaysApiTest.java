package com.jevillac07.rxjava.observer;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DelaysApiTest {

    @Test
    @DisplayName("Delay with APIs")
    void delaysWithApisTest() {
        // Crear un Observable que simula una llamada a una API
        Observable<String> apiCall = Observable.fromCallable(() -> {
            // Simular retraso de llamada a API
            Thread.sleep(1000);
            return "Hello from API!";
        });

        // Crear un Observable que simula una llamada a una API
        Observable<String> apiCallTwo = Observable.fromCallable(() -> {
            // Simular retraso de llamada a API
            Thread.sleep(2000);
            return "Hello from API tWo!";
        });

        // Suscribirse al Observable
        apiCall
            .subscribeOn(Schedulers.io()) // Ejecutar la llamada en el Scheduler.io()
            .observeOn(Schedulers.single()) // Procesar el resultado en un solo hilo
            .subscribe(
                result -> System.out.println("Received: " + result), // Manejar el resultado
                Throwable::printStackTrace, // Manejar errores
                () -> System.out.println("Completed") // Acción al completar
            );

        // Para mantener la aplicación en ejecución (en un entorno real, esto no es necesario)
        try {
            System.out.println("Initialized");
            Thread.sleep(3000); // Mantener la aplicación en ejecución para ver el resultado
            System.out.println("Final");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
