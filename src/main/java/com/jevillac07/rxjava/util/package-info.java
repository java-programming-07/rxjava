/**
 * Util package.
 *
 * @author Juan Villaorduna
 * @version 1.0.0
 * @since 1.0.0
 */
package com.jevillac07.rxjava.util;
