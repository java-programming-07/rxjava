package com.jevillac07.rxjava.util;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Read file utility.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
public final class ReadFile {

  private ReadFile() {

  }

  /** Object mapper. */
  private static final ObjectMapper MAPPER = new ObjectMapper();

  static {
    MAPPER.findAndRegisterModules();
  }

  /**
   * readDataFromFile: read data from a file.
   * @param fileName file name
   * @param className class name
   * @param <T> This describes a type parameter
   * @return a generic object
   */
  public static <T> T readDataFromFile(
    final String fileName, final Class<T> className) {
    try {
      return MAPPER.readValue(
        ReadFile.class.getClassLoader().getResourceAsStream(fileName),
        className);
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }
}
