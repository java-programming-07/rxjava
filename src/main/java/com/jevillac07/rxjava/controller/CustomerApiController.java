package com.jevillac07.rxjava.controller;

import com.jevillac07.openapi.api.CustomerApi;
import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.openapi.model.SaveCustomerApiResponse;
import com.jevillac07.rxjava.service.CustomerApiService;
import io.reactivex.rxjava3.core.Maybe;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * Customer controller.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
@RestController
@AllArgsConstructor
public class CustomerApiController implements CustomerApi {
  /** Customer service auto wired. */
  private CustomerApiService customerApiService;

  /**
   * saveCustomer: save a customer controller.
   * @param saveCustomerApiRequest object representing the request
   * @return a {@link SaveCustomerApiResponse} of type {@link Maybe}
   */
  @Override
  public Maybe<ResponseEntity<SaveCustomerApiResponse>> saveCustomer(
    final SaveCustomerApiRequest saveCustomerApiRequest) {
    return customerApiService.saveCustomer(saveCustomerApiRequest)
      .toMaybe()
      .map(ResponseEntity::ok);
  }
}
