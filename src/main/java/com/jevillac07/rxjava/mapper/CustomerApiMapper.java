package com.jevillac07.rxjava.mapper;

import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.openapi.model.SaveCustomerApiResponse;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyRequest;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

/**
 * Customer mapper.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
@Mapper(
  componentModel = "spring",
  imports = { Instant.class, LocalDate.class, ZoneId.class }
)
public interface CustomerApiMapper {
  /**
   * saveCustomerApiToSaveCustomerProxy: method that converts
   * to SaveCustomerProxyRequest.
   * @param request object representing the request
   * @return SaveCustomerProxyRequest object
   */
  @Mapping(
    target = "phone",
    expression = "java(getPhone(request.getPhoneType().getValue(), "
      + "request.getPhoneNumber()))")
  @Mapping(
    target = "date",
    expression =
      "java(LocalDate.ofInstant(Instant.now(), ZoneId.systemDefault()))")
  SaveCustomerProxyRequest saveCustomerApiToSaveCustomerProxy(
    SaveCustomerApiRequest request);

  /**
   * getPhone: get a phone with country code.
   * @param phoneType phone type (mobile, home, etc.)
   * @param phoneNumber phone number
   * @return a phone number
   */
  default String getPhone(String phoneType, String phoneNumber) {
    String countryCode = (phoneType.equalsIgnoreCase("mobile")) ? "+51" : "0";

    return countryCode + " " + phoneNumber;
  }

  /**
   * saveCustomerProxyToSaveCustomerApi: method that converts
   * to SaveCustomerApiResponse.
   * @param request object representing the request
   * @return SaveCustomerApiResponse object
   */
  @Mapping(
    source = "request",
    target = "age",
    qualifiedByName = "getAge")
  SaveCustomerApiResponse saveCustomerProxyToSaveCustomerApi(
    SaveCustomerProxyResponse request);

  /**
   * getAge: get the customer age.
   * @param request object representing the request
   * @return an age in string format
   */
  @Named("getAge")
  default Integer getAge(SaveCustomerProxyResponse request) {
    return Period.between(request.getBirthday(), LocalDate.now())
      .getYears();
  }
}
