package com.jevillac07.rxjava.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Global exception handler.
 * @author Juan Villaorduna
 * @version 2022/10/10
 */
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
  /**
   * handleExceptionGeneral: return a internal server error.
   * @param ex object representing an exception
   * @return a internal server error - 500
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleExceptionGeneral(final Exception ex) {
    String error = "Server error: " + ex.getMessage();
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * handleExceptionGeneral: return a internal server error.
   * @param ex object representing an exception
   * @param body is a body of the exception
   * @param headers is a list of headers
   * @param status is a http status
   * @param request is the web request
   * @return a bad request - 400
   */
  @Override
  @SuppressWarnings("NullableProblems")
  protected ResponseEntity<Object> handleExceptionInternal(
    final Exception ex, final Object body, final HttpHeaders headers,
    final HttpStatus status, final WebRequest request) {
    String error = "Bad request: " + ex.getMessage();

    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }
}
