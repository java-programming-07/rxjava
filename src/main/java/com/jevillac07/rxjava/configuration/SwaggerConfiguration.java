package com.jevillac07.rxjava.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Swagger configuration.
 * @author Juan Villaorduna
 * @version 2022/10/31
 */
@Configuration
public class SwaggerConfiguration {
  /**
   * api: Swagger configuration.
   * @return a Docket
   */
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .useDefaultResponseMessages(false)
      .select()
      .apis(RequestHandlerSelectors.basePackage("com.jevillac07.rxjava"))
      .paths(PathSelectors.any())
      .build();
  }
}
