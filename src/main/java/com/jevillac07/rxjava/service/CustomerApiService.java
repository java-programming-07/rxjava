package com.jevillac07.rxjava.service;

import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.openapi.model.SaveCustomerApiResponse;
import io.reactivex.rxjava3.core.Single;

/**
 * Customer service interface.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
public interface CustomerApiService {
  /**
   * saveCustomer: save a customer with service.
   * @param saveCustomerApiRequest object representing the request
   * @return a customer
   */
  Single<SaveCustomerApiResponse> saveCustomer(
    SaveCustomerApiRequest saveCustomerApiRequest);
}
