package com.jevillac07.rxjava.service.imp;

import com.jevillac07.openapi.model.SaveCustomerApiRequest;
import com.jevillac07.openapi.model.SaveCustomerApiResponse;
import com.jevillac07.rxjava.mapper.CustomerApiMapper;
import com.jevillac07.rxjava.repository.CustomerRepository;
import com.jevillac07.rxjava.service.CustomerApiService;
import io.reactivex.rxjava3.core.Single;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Customer service implement.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
@Service
@Slf4j
@AllArgsConstructor
public final class CustomerApiServiceImp implements CustomerApiService {
  /** Customer repository with feign. */
  private CustomerRepository customerRepository;

  /** Customer mapper. */
  private CustomerApiMapper customerApiMapper;

  @Override
  public Single<SaveCustomerApiResponse> saveCustomer(
    final SaveCustomerApiRequest saveCustomerApiRequest) {

    return Single.just(customerApiMapper
        .saveCustomerApiToSaveCustomerProxy(saveCustomerApiRequest))
      .flatMap(customerRepository::saveCustomer)
      .map(customerApiMapper::saveCustomerProxyToSaveCustomerApi);
  }
}
