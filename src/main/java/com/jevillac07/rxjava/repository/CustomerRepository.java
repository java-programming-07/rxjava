package com.jevillac07.rxjava.repository;

import com.jevillac07.rxjava.model.provider.SaveCustomerProxyRequest;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyResponse;
import io.reactivex.rxjava3.core.Single;

/**
 * Customer repository interface.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
public interface CustomerRepository {
  /**
   * saveCustomer: save a customer with repository.
   * @param saveCustomerProxyRequest object representing the request
   * @return a customer proxy
   */
  Single<SaveCustomerProxyResponse> saveCustomer(
    SaveCustomerProxyRequest saveCustomerProxyRequest);
}
