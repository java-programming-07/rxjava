package com.jevillac07.rxjava.repository.imp;

import com.jevillac07.rxjava.model.provider.SaveCustomerProxyRequest;
import com.jevillac07.rxjava.model.provider.SaveCustomerProxyResponse;
import com.jevillac07.rxjava.repository.CustomerRepository;
import com.jevillac07.rxjava.util.ReadFile;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

/**
 * Customer repository implement.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
@Repository
@Slf4j
@AllArgsConstructor
public final class CustomerRepositoryImp implements CustomerRepository {
  @Override
  public Single<SaveCustomerProxyResponse> saveCustomer(
    final SaveCustomerProxyRequest saveCustomerProxyRequest) {
    SaveCustomerProxyResponse response = ReadFile.readDataFromFile(
      "mock/save-customer-proxy-response.json",
      SaveCustomerProxyResponse.class);
    return Single.just(response)
      .doOnSubscribe(disposable ->
        log.info("doOnSubscribe > saveCustomer: " + disposable.isDisposed()))
      .subscribeOn(Schedulers.io())
      .doOnSuccess(saveCustomerResponse ->
        log.info("doOnSuccess   > saveCustomer: "
          + saveCustomerResponse.toString()));
  }
}
