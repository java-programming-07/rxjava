package com.jevillac07.rxjava.model.provider;

import lombok.Data;

import java.time.LocalDate;

/**
 * Save customer proxy request model.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
@Data
public class SaveCustomerProxyRequest {
  /** Customer name. */
  private String name;

  /** Customer phone with country code. */
  private String phone;

  /** Customer birthday. */
  private LocalDate birthday;

  /** Registration date. */
  private LocalDate date;
}
