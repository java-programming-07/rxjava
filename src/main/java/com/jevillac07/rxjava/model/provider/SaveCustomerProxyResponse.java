package com.jevillac07.rxjava.model.provider;

import lombok.Data;

import java.time.LocalDate;

/**
 * Save customer proxy response model.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
@Data
public class SaveCustomerProxyResponse {
  /** Identified. */
  private Long id;

  /** Customer name. */
  private String name;

  /** Customer phone. */
  private String phone;

  /** Customer address. */
  private String address;

  /** Customer birthday. */
  private LocalDate birthday;

  /** Registration date. */
  private LocalDate date;
}
