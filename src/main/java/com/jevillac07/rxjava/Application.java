package com.jevillac07.rxjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application initialization.
 * @author Juan Villaorduna
 * @version 2022/10/03
 */
@SpringBootApplication
public class Application {
  /**
   * Our main method. Some kind of handy description goes here.
   * @param args The command line arguments.
   **/
  public static void main(final String[] args) {
    new Application().run(args);
  }

  private void run(final String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
